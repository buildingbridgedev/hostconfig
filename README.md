# README #

Repository for the standard configuration of Building Bridge Biznet servers

### Configuration Overview ###

* Primary OS: CentOS
* Version: 7 & 8
* 

### Configuration Components ###

* System Update
* YUM Repositories
* Software Installation
* Software configuration
* Database configuration
* System Settings Update

### YUM Repositories ###

* Webmin Management Software
* Bitrix Code & Source
* mySQL
* 

### Software Installation ###

* Repo owner or admin
* Other community or team contact

### Software Configuration ###

* Repo owner or admin

### Database Configuration ###

* Repo owner or admin

### System Configuration ###

* Repo owner or admin

### Misc. Steps ###

* Repo owner or admin